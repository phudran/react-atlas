import { createGlobalStyle } from 'styled-components'

const GlobalStyle = createGlobalStyle`
  html, body, #root, #root > * {
    width: 100%;
    height: 100%;
  }
  body {
    background-color: #f5f6f5;
  }
`;

export default GlobalStyle;