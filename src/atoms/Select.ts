/* based on https://codesandbox.io/s/qk8r8l7w54 */

import styled from 'styled-components';

export const Select = styled.select`
  width: 200px;
  height: 35px;
  background: white;
  color: #686f7a;
  font-size: 14px;
  border: 1px solid #d9d9d9;
  option {
    color: #686f7a;
    background: white;
    display: flex;
    white-space: pre;
    min-height: 20px;
    padding: 1px 2px;
  }
`;
