import styled from "styled-components";

interface ButtonProps {
    readonly primary?: boolean;
    readonly justIcon?: boolean;
}
export const Button = styled.button<ButtonProps>`
    display: inline-flex;
    align-items: center;
    background: ${props => (props.primary ? "#80b538" : "#f2f2f2")};
    border-color: ${props => (props.primary ? "#80b538" : "#d9d9d9")};
    color: ${props => (props.primary ? "#ffffff" : "#212529")};
    border-style: solid;
    border-width: 1;
    font-size: 14px;
    margin: 0,;
    padding: 7px 10px;
    border-radius: 3px;
    cursor: pointer;
    .icon { margin-right: ${props => (props.justIcon ? "0" : "10px")}; }
    &:hover {
        cursor: pointer;
        background: ${props => (props.primary ? "#5D8228" : "#d9d9d9")};
        borderColor: ${props => (props.primary ? "#5D8228" : "#d9d9d9")};
        color: ${props => (props.primary ? "#ffffff" : "#212529")};
    }
`;
