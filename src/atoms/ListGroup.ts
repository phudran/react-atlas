import styled from "styled-components";

interface ListGroupProps {
    readonly nested?: boolean;
    readonly borderTop?: boolean;
    readonly bottom?: boolean;
};

export const ListGroup = styled.ul<ListGroupProps>`
    display: block;
    width: 100%;
    margin: 0;
    margin-left: ${props => (props.nested ? '30px' : '0')};
    padding: ${props => (props.nested ? '0' : '20px')};
    border-top: ${props => (props.borderTop ? '1px solid #5b5f67' : '0 none')};
    position: ${props => (props.bottom ? 'absolute' : 'relative')};
    left: 0;
    bottom: 0;
    right: 0;
    list-style-type: none;
`;

interface ListGroupItemProps {
    readonly active?: boolean;
    readonly tag?: string;
    readonly href?: string;
};

export const ListGroupItem = styled.li<ListGroupItemProps>`
    display: flex;
    align-items: center;
    padding: 10px;
    border: 0 none;
    border-radius: 5px;
    background-color: #2b313c;
    color: #ffffff;
    background-color: ${props => (props.active ? '#5b5f67' : 'transparent')};
    .icon { margin-right: 10px }
`;

