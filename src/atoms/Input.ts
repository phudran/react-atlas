import styled from 'styled-components';

interface FormGroupProps {
    readonly inline?: boolean;
};

export const FormGroup = styled.div<FormGroupProps>`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: flex-start;
`;

export const Label = styled.label`
  font-size: 14px;
  color: gray;
  margin-right: 10px;
`;
