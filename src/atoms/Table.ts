/* based on https://github.com/atahani/styled-table  */

//import React from "react";
import styled from "styled-components";

interface TableProps {
    readonly rounded: boolean;
    readonly bordered: boolean;
    readonly striped: boolean;
};

export const Table = styled.table<TableProps>`
  width: 100%;
  border-collapse: collapse;
  margin-bottom: 0;
  color: #686f7a;
  border-radius: ${props => (props.rounded ? '5px !important' : '0')};
  thead {
    background-color: #2b313c;
    color: #ffffff;
    tr {
      th {
        border-left: ${props => (props.bordered ? '1px solid #ffffff' : '0 none')};
        border-right: ${props => (props.bordered ? '1px solid #ffffff' : '0 none')};
        &:first-child { border-left: ${props => (props.bordered ? '1px solid #2b313c' : '0 none')}; }
        &:last-child { border-right: ${props => (props.bordered ? '1px solid #2b313c' : '0 none')}; }
      }
    }
  }
  tbody {
    tr {
      &:nth-child(even) { background-color: ${props => (props.striped ? '#f2f2f2' : 'transparent')}; }
      td { border: ${props => (props.bordered ? '1px solid #d9d9d9' : '0 none')}; }
    }
  }
`;

interface ThTdProps {
    readonly align?: string;
    readonly hideOnDesktop?: boolean;
    readonly hideOnTablet?: boolean;
    readonly hideOnPhone?: boolean;
};

export const Th = styled.th<ThTdProps>`
  text-align: ${props => (props.align ? props.align : "left")};
  padding: 5px;
  vertical-align: middle;
  font-size: 14px;
  font-weight: normal;
  color: #ffffff;
`;

export const Td = styled.td<ThTdProps>`
  text-align: ${props => (props.align ? props.align : "left")};
  padding: 2px 5px;
  vertical-align: middle;
`;

export const Tr = styled.tr`
  border-bottom: 1px solid #d9d9d9;
`;

