import styled from "styled-components";

interface AnchorProps {
    readonly icon?: boolean;
    readonly light?: boolean;
}
export const Anchor = styled.button<AnchorProps>`
    display: inline-flex;
    align-items: center;
    justify-content: center;
    padding: 7px 10px;
    margin: 0,;
    font-size: 16px;
    border: 0 none;
    border-radius: 3px;
    cursor: pointer;
    background: transparent;
    text-transform: ${props => (props.icon ? "none" : "underline")};
    color: ${props => (props.light ? "#212529" : "#ffffff")};
    &:hover {
        background: ${props => (props.light ? "#d9d9d9" : "#212529")};
        color: ${props => (props.light ? "#212529" : "#ffffff")};
    }
`;
