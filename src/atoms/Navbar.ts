import styled from "styled-components";

interface NavbarProps {
    readonly fixed?: boolean;
};

export const Navbar = styled.nav<NavbarProps>`
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    position: ${props => (props.fixed ? 'fixed' : 'relative')};
    left: 0;
    top: ${props => (props.fixed ? '60px' : 0)};
    right: 0;
    padding: 12px 20px;
    border: 1px solid #d9d9d9;
    border-left: 0;
    border-right: 0;
    background-color: #ffffff;
    h1 {
        margin-bottom: 0;
        font-size: 16px;
        font-weight: normal;
    }
`;