import styled from "styled-components";

interface MenuProps {
    readonly borderTop?: boolean;
    readonly bottom?: boolean;
};

export const Menu = styled.ul<MenuProps>`
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 100%;
    height: 50px;
    margin: 0;
    padding: 0 10px;
    border-top: ${props => (props.borderTop ? '1px solid #d9d9d9' : '0 none')};
    position: ${props => (props.bottom ? 'fixed' : 'relative')};
    left: 0;
    bottom: 0;
    right: 0;
    background-color: #ffffff;
`;

interface MenuItemProps {
    readonly active?: boolean;
    readonly tag?: string;
    readonly href?: string;
};

export const MenuItem = styled.li<MenuItemProps>`
    display: inline-flex;
    align-items: center;
    justify-content: center;
    height: 50px;
    padding: 10px;
    border: 0 none;
    cursor: pointer;
    color: ${props => (props.active ? '#1990ea' : '#777777')};
    &:hover { color: #1990ea; }
`;