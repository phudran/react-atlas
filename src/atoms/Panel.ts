import styled from "styled-components";

interface PanelProps {
    readonly shadow?: boolean;
    readonly bordered?: boolean;
};

export const Panel = styled.div<PanelProps>`
    margin-bottom: 15px;
    padding: 10px;
    border: ${props => (props.bordered ? '1px solid #d9d9d9' : '0 none')};
    border-radius: 3px;
    background-color: #ffffff;
    box-shadow: ${props => (props.shadow ? '0px 2px 5px 2px rgba(0,0,0,0.07)' : 'none')};
`;

export const PanelHeader = styled.div`
    display: flex !important;
    flex-wrap: nowrap;
    justify-content: space-between;
    align-items: center;
    width: 100%;
    padding-bottom: 10px;
    & > span { font-size: 13px; }
    & > a { 
        font-size: 16px;
        color: #1990ea;
    }
`;

export const PanelBody = styled.div`
    & > span { font-size: 13px; }
    hr { 
        border: 0 none;
        border-top: 1px solid #F2F2F2;
    }
`;

