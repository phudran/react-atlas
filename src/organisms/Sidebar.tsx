import React from 'react';
import { ListGroup, ListGroupItem } from '../atoms';
import { FaPowerOff } from 'react-icons/fa';
import styled from "styled-components";

export const Sidebar = () => {

    return (
        <Aside>
            <img alt="logo" style={{margin: '30px 0'}} src="data:image/svg+xml;charset=UTF-8,%3Csvg%20width%3D%22200%22%20height%3D%22150%22%20xmlns%3D%22http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg%22%20viewBox%3D%220%200%20200%20150%22%20preserveAspectRatio%3D%22none%22%3E%0A%20%20%20%20%20%20%3Cdefs%3E%0A%20%20%20%20%20%20%20%20%3Cstyle%20type%3D%22text%2Fcss%22%3E%0A%20%20%20%20%20%20%20%20%20%20%23holder%20text%20%7B%0A%20%20%20%20%20%20%20%20%20%20%20%20fill%3A%20%23ffffff%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20font-family%3A%20sans-serif%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20font-size%3A%2024px%3B%0A%20%20%20%20%20%20%20%20%20%20%20%20font-weight%3A%20400%3B%0A%20%20%20%20%20%20%20%20%20%20%7D%0A%20%20%20%20%20%20%20%20%3C%2Fstyle%3E%0A%20%20%20%20%20%20%3C%2Fdefs%3E%0A%20%20%20%20%20%20%3Cg%20id%3D%22holder%22%3E%0A%20%20%20%20%20%20%20%20%3Crect%20width%3D%22100%25%22%20height%3D%22100%25%22%20fill%3D%22%235b5f67%22%3E%3C%2Frect%3E%0A%20%20%20%20%20%20%20%20%3Cg%3E%0A%20%20%20%20%20%20%20%20%20%20%3Ctext%20text-anchor%3D%22middle%22%20x%3D%2250%25%22%20y%3D%2250%25%22%20dy%3D%22.3em%22%3ELOGO%3C%2Ftext%3E%0A%20%20%20%20%20%20%20%20%3C%2Fg%3E%0A%20%20%20%20%20%20%3C%2Fg%3E%0A%20%20%20%20%3C%2Fsvg%3E" />
            {/* https://placeholderimage.dev/ */}
            <ListGroup borderTop>
                <ListGroupItem active tag="button" href="#">Uživatelé</ListGroupItem>
                <ListGroup nested>
                    <ListGroupItem tag="a" href="#">Vytvořit uživatele</ListGroupItem>
                    <ListGroupItem tag="a" href="#">Hromadný import</ListGroupItem>
                </ListGroup>
                <ListGroupItem tag="a" href="#">Licence</ListGroupItem>
                <ListGroupItem tag="a" href="#">Export</ListGroupItem>
                <ListGroupItem tag="a" href="#">Nápověda</ListGroupItem>
            </ListGroup>
            <ListGroup bottom>
                <ListGroupItem tag="a" href="#"><FaPowerOff className="icon" /> Odhlásit se</ListGroupItem>
            </ListGroup>
        </Aside>
    );
}

const Aside = styled.aside`
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: flex-start;
    width: 300;
    height: 100%;
    background-color: #2b313c;
    img { border-radius: 5px; } 
`;
