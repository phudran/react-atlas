import React from 'react';
import {Container, Button, Table, Tr, Th, Navbar, Select, Label, FormGroup, Checkbox, Panel, PanelHeader, PanelBody} from '../atoms';
import {TableRow} from '../molecules';
import {FaPlus} from 'react-icons/fa';
import {SelectOptions, Users} from "../types";

const options: SelectOptions = [
    {value: 'role', label: 'Nastavit roli'},
    {value: 'licence', label: 'Nastavit licenci'},
    {value: 'email', label: 'Upravit e-mail'},
];

const users: Users = [
    {id: 123456, email: 'slosar@atlasgroup.cz', name: 'Martin Šlosar', license: ['Licence 1', 'Licence 2'], selected: true},
    {id: 234567, email: 'trehak@atlasgroup.cz', name: 'Tomáš Řehák', license: ['Licence 3'], selected: false},
    {id: 345678, email: 'orlik@atlasgroup.cz', name: 'Vojta Orlík', license: ['Licence 2'], selected: false},
    {id: 456789, email: 'novak@atlasgroup.cz', name: 'Petr Novák', license: ['Licence 2', 'Licence 3'], selected: false},
    {id: 567890, email: 'rychly@atlasgroup.cz', name: 'Jan Rychlý', license: ['Licence 1', 'Licence 3'], selected: false}
];

interface Props {

}

interface State {
    users: Users,
    allSelected: boolean
}

export class Main extends React.Component<Props, State> {

    state: State = {
        users: users,
        allSelected: false
    }

    selectUser = (id: number, value: boolean) => {
        this.setState({
            users: this.state.users.map((user) => {
                return {...user, selected: user.id === id ? value : user.selected}
            })
        })
    }

    selectAll = () => {
        this.setState({
            allSelected: !this.state.allSelected,
            users: this.state.users.map((user) => {
                return {...user, selected: !this.state.allSelected}
            })
        })
    }

    render() {

        let {users} = this.state

        return (
            <main>
                <Navbar>
                    <h1>Seznam uživatelských účtů</h1>
                </Navbar>
                <Container>
                    <Panel>
                        <PanelHeader>
                            <FormGroup inline>
                                <Label>Pro vybrané uživatele</Label>
                                <Select>
                                    {options && options.map((option) => <option key={option.value}
                                                                                value={option.value}>{option.label}</option>)}
                                </Select>
                            </FormGroup>
                            <Button primary><FaPlus className="icon"/> Vytvořit uživatele</Button>
                        </PanelHeader>
                        <PanelBody>
                            <Table striped bordered rounded>
                                <thead>
                                <Tr>
                                    <Th align="center"><Checkbox onChange={this.selectAll} checked={this.state.allSelected}/></Th>
                                    <Th>ID</Th>
                                    <Th>E-mail</Th>
                                    <Th>Jméno</Th>
                                    <Th>Licence</Th>
                                </Tr>
                                </thead>
                                <tbody>
                                {users && users.map((user) => <TableRow key={user.id} onCheckChange={this.selectUser}
                                                                        data={user}/>)}
                                </tbody>
                            </Table>
                        </PanelBody>
                    </Panel>
                </Container>
            </main>
        );

    }

}
