export { Main } from "./Main";
export { Sidebar } from "./Sidebar";
export { HeaderMobile } from "./HeaderMobile";
export { FooterMobile } from "./FooterMobile";
export { MainMobile } from "./MainMobile";
