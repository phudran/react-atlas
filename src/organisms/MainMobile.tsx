import React from 'react';
import {Container, Button, Navbar} from '../atoms';
import {PanelRow} from '../molecules';
import {FaPlus} from 'react-icons/fa';
import {Users} from "../types";
import styled from "styled-components";

const users: Users = [
    {id: 123456, email: 'slosar@atlasgroup.cz', name: 'Martin Šlosar', license: ['Licence 1', 'Licence 2'], selected: true},
    {id: 234567, email: 'trehak@atlasgroup.cz', name: 'Tomáš Řehák', license: ['Licence 3'], selected: false},
    {id: 345678, email: 'orlik@atlasgroup.cz', name: 'Vojta Orlík', license: ['Licence 2'], selected: false},
    {id: 456789, email: 'novak@atlasgroup.cz', name: 'Petr Novák', license: ['Licence 2', 'Licence 3'], selected: false},
    {id: 567890, email: 'rychly@atlasgroup.cz', name: 'Jan Rychlý', license: ['Licence 1', 'Licence 3'], selected: false}
];

interface Props {

}

interface State {
    users: Users
}

export class MainMobile extends React.Component<Props, State> {

    state: State = {
        users: users
    }


    render() {

        let {users} = this.state

        return (
            <Main>
                <Navbar fixed>
                    <h1>Seznam uživatelských účtů</h1>
                    <Button primary justIcon><FaPlus className="icon"/></Button>
                </Navbar>
                <Container>
                    {users && users.map((user) => <PanelRow key={user.id} data={user}/>)}
                </Container>
            </Main>
        );

    }

}

const Main = styled.main`
    margin: 110px 0 50px 0;
    overflow-x: hidden;
    overflow-y: auto; 
`;