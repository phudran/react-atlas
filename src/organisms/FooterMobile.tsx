import React from 'react';
import { Menu, MenuItem } from '../atoms';

export class FooterMobile extends React.Component {

    render() {

        return (
            <Menu bottom borderTop>
                <MenuItem active>Uživatelé</MenuItem>
                <MenuItem>Licence</MenuItem>
                <MenuItem>Export</MenuItem>
                <MenuItem>Nápověda</MenuItem>
            </Menu>
        );

    }

}
