import styled from "styled-components";

export const LayoutSidebar = styled.div`
    display: flex;
    flex-direction: row;
    align-items: stretch;
    justify-content: flex-start;
    aside { width: 300px; }
    main { width: calc(100% - 300px); }
`;