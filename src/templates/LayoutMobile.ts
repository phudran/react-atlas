import styled from "styled-components";

export const LayoutMobile = styled.div`
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: flex-start;
    header { }
    footer { }
`;
