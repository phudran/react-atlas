export type User = {
    id: number,
    email: string,
    name: string,
    license: Array<string>,
    selected: boolean
}

export type Users = Array<User>

export type SelectOption = {
    value: string,
    label?: string
}

export type SelectOptions = Array<SelectOption>
