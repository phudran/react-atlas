import React, { Fragment } from "react";
import { LayoutSidebar, LayoutMobile } from './templates';
import { HeaderMobile, FooterMobile, Sidebar, Main, MainMobile } from "./organisms";
import GlobalStyle from "./utils/GlobalStyle";
import Media from 'react-media';

class App extends React.Component {

    render() {
        return (
            <Media queries={{
                mobile: "(max-width: 599px)",
                other: "(min-width: 600px)"
            }}>
                {matches => (
                    <Fragment>
                        <GlobalStyle/>
                        {matches.mobile &&
                        <Fragment>
                            <LayoutMobile>
                                <HeaderMobile/>
                                <MainMobile/>
                                <FooterMobile/>
                            </LayoutMobile>
                        </Fragment>
                        }
                        {matches.other &&
                        <Fragment>
                            <LayoutSidebar>
                                <Sidebar/>
                                <Main/>
                            </LayoutSidebar>
                        </Fragment>
                        }
                    </Fragment>
                )}
            </Media>
        )
    }
}

export default App;

