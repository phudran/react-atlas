import React from "react";
import {Panel, PanelBody, PanelHeader} from '../atoms';
import {User} from "../types";

interface Props {
    data: User
}

export class PanelRow extends React.Component<Props> {


    render() {

        let {data} = this.props

        return (
            <Panel shadow>
                <PanelHeader>
                    <a href="mailto:{data && data.email}">{data && data.email}</a>
                    <span><strong>ID:</strong> {data && data.id}</span>
                </PanelHeader>
                <PanelBody>
                    <span><strong>ID:</strong> {data && data.name}</span>
                    <hr/>
                    <span><strong>Licence:</strong> {data && data.license.join(', ')}</span>
                </PanelBody>
            </Panel>
        );

    }

}
