import React from "react";
import {Button, Tr, Td, Checkbox} from '../atoms';
import {User} from "../types";

interface Props {
    data: User,
    onCheckChange: ((id: number, value: boolean) => void)
}

export class TableRow extends React.Component<Props> {

    onCheckChange = (value : boolean) => {
        this.props.onCheckChange(this.props.data.id, value)
    }

    render() {

        let {data} = this.props

        return (
            <Tr>
                <Td align="center"><Checkbox checked={data.selected} onChange={this.onCheckChange}/></Td>
                <Td>{data && data.id}</Td>
                <Td><a href="mailto:{data && data.email}">{data && data.email}</a></Td>
                <Td>{data && data.name}</Td>
                <Td>
                    <div
                        style={{display: 'flex', width: '100%', justifyContent: 'space-between', alignItems: 'center'}}>
                        <span>{data && data.license.join(', ')}</span>
                        <Button>Detail</Button>
                    </div>
                </Td>
            </Tr>
        );

    }

}
